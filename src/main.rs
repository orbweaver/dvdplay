use clap::{self, Parser};

mod mpv;

/// Default DVD device
const DEVICE: &str = "/dev/sr0";

#[derive(Parser)]
#[command(version, about)]
struct Arguments {
    /// Title to play from DVD (1 - 99)
    title: i32,

    /// DVD device to use
    #[arg(short, long)]
    device: Option<String>,

    /// Add options for deblocking badly-encoded DVDs
    #[arg(long)]
    deblock: bool,

    /// Add options for smoothing animation content
    #[arg(long)]
    anim: bool,

    /// Other arguments to mpv
    #[arg(trailing_var_arg=true, num_args=.., allow_hyphen_values=true)]
    mpv_args: Vec<String>,
}

fn main() {
    let mut args = Arguments::parse();

    // Set up arguments
    let mut mpv_args = Vec::new();
    if args.deblock {
        mpv_args.push(String::from("--vf=lavfi=[pp=ha/va]"));
        mpv_args.push(String::from("--deband-grain=192"));
    }
    if args.anim {
        mpv_args.push(String::from("--scale=bicubic"));
        mpv_args.push(String::from("--deband-grain=192"));
        mpv_args.push(String::from("--vf=lavfi=[pp=dr]"));
    }
    mpv_args.append(&mut args.mpv_args);

    // Invoke the player
    let player = mpv::Player::new(args.device.unwrap_or_else(|| String::from(DEVICE)));
    player.play(args.title, &mpv_args);
}
