use std::ffi::OsStr;

// Run a program with arguments
fn run<'a, P, I, A>(program: P, args: I)
where
    P: AsRef<OsStr>,
    I: IntoIterator<Item = A>,
    A: AsRef<OsStr>,
{
    // Construct and print the command
    let mut command = std::process::Command::new(program);
    command.args(args);
    eprintln!("+ {:?}", command);

    // Execute the command
    let mut child = command.spawn().expect("failed to execute process");
    let result = child.wait().expect("failed to wait on process");
    if !result.success() {
        eprintln!("process exited with error:\n{}", result);
    }
}

/// Structure managing playback from a given DVD device
pub struct Player {
    device: String,
}

impl Player {
    /// Construct and set device
    pub fn new(device: impl Into<String>) -> Player {
        Player { device: device.into() }
    }

    fn configure_drive(&self) {
        run("sdparm", [self.device.as_ref(), "--set=RC,RRC=0"]);
    }

    pub fn play(&self, title: i32, user_args: &Vec<String>) {
        // Set up the drive for continuous reading if possible
        self.configure_drive();

        // Apply the idiotic title mangling for MPV. MPV pretends that DVD title numbers
        // start from 0, even though they actually start from 1 (according to the DVD
        // spec, the underlying libdvd/libdvdnav and every other DVD-related tool in
        // existence.)
        let mangled_title = title - 1;

        // Run mpv itself
        let url = format!("dvd://{}", mangled_title);
        let device = format!("--dvd-device={}", self.device);
        let args = [&url, &device, "--hr-seek=yes"];
        run("mpv", args.into_iter().chain(user_args.iter().map(|s| s.as_ref())));
    }
}
